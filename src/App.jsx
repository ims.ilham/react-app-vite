import React from "react"

// bisa menggunakan Class component 
// class Button extends React.Component{
//   render(){
//     return <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full">
//       Button White
//     </button>
//   }
// }

// bisa menggunakan Functional component
// function ButtonBlack() {
//   return <button className="bg-black hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full">
//     Button Black
//   </button>
// }

// menggunakan ArrowFunction

// const ButtonRed = () => {
//   return <button className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded-full">
//     Button Red
//   </button>
// }


// misalkan ingin mempunyai component satu saja cuman bisa di pakai oleh banyak 
const ButtonPrimer = (props) => {
  const {children, variant = "bg-yellow"} = props;
  return <button className={`h-10 px-6 font-semibold rounded-md ${variant} text-white`} type="submit">
    {children}
  </button>
}


function App() {
  return <div className="flex justify-center bg-blue-600 min-h-screen items-center">
    <div className="flex gap-x-3">
      {/* <Button />
      <Button />
      <Button />
      <ButtonBlack />
      <ButtonRed /> */}
      <ButtonPrimer variant="bg-red-700">normalday</ButtonPrimer>
      <ButtonPrimer variant="bg-blue-500">Logut</ButtonPrimer>
      <ButtonPrimer/>
    </div>
  </div>
}


export default App



// import { useState } from 'react'
// import reactLogo from './assets/react.svg'
// import viteLogo from '/vite.svg'
// import './App.css'